const http = require('http')
const fs = require('fs');
const url = require('url');
const logsFile = './logs.json'

module.exports = () => {
    fs.mkdir('./file',function(e){
        if(!e || (e && e.code === 'EEXIST')){
        } else {
            throw e
        }
    });
    const writeLogs = (messageText) => {
        fs.readFile('./logs.json', 'utf-8', function (err, content) {
            if (err) throw err
            const { logs } = JSON.parse(content)
            logs.push({ 'message': messageText, time: Date.now() })
            const data = JSON.stringify({ logs }, null, 4);
            fs.writeFile('./logs.json', data, function (err) {
                if (err) throw err
            })
        })
    }

    const createLogFile = (response) =>{
        let dataToObj = JSON.stringify({"logs": []},null,4)
        fs.writeFile(`./logs.json`, dataToObj, function (err, content) {
            if (err) {
                response.writeHead(404, { 'Content-type': 'text/html' })
                response.end(`Something goes wrong`)
            }
        });
    }
    if (!fs.existsSync(logsFile)) {
        createLogFile()
    }
    
    const isCorrectFileExtension = (response,{filename}) =>{
        let isFilenameExtension = filename.match(/\.\w+$/g) 
        let pointsInFilename = filename.match(/\./g)
        if(!isFilenameExtension|| !pointsInFilename || pointsInFilename.length>1){
            response.writeHead(400, { 'Content-type': 'text/html' })
            response.end("Wrong file extention")
            return false
        }return true
    }


    http.createServer(function (request, response) {
        const { pathname, query } = url.parse(request.url, true)
        const copyedPath = pathname
        if (request.method === 'GET') {
            if (pathname === '/logs') {
                if (!fs.existsSync(logsFile)) {
                    createLogFile(response)
                }
                if (!query.from || !query.to) {
                    fs.readFile("./logs.json", 'utf-8', (err, content) => {
                        if (err) {
                            response.writeHead(404, { 'Content-type': 'text/html' })
                            response.end(`File "logs.json" not found`)
                            return
                        } else {
                            response.writeHead(200, { 'Content-type': 'application/json' })
                            response.end(content)
                            
                            return
                        }
                    })
                } else {
                    fs.readFile(`./logs.json`, 'utf-8', (err, content) => {
                        if (err) {
                            response.writeHead(400, { 'Content-type': 'text/html' })
                            response.end(`Bad request`)
                            return
                        } else {
                            let logs = JSON.parse(content)
                            let filtredData = logs.logs.filter(item => ((item.time >= parseInt(query.from) && (item.time <= parseInt(query.to)))))
                            let data = JSON.stringify({"logs": filtredData}, null, 4)
                            response.writeHead(200, { 'Content-type': 'application/json' })
                            response.end(data)
                            return
                        }
                    })
                }

            } else if (copyedPath.replace(/\/[^/]+$/, '') == '/file') {
                fs.readFile(`.${pathname}`, (err, content) => {
                    if (err) {
                        response.writeHead(400, { 'Content-type': 'text/html' })
                        response.end(`Files named "${pathname}" not found`)
                        return
                    } else {
                        let copyPathName = pathname
                        let originFileName = copyPathName.match(/\/[^\/]+$/g).join('').slice(1)
                        writeLogs(`The file ${originFileName} was shown to the user`)
                        response.writeHead(200, { 'Content-type': 'text/html' })
                        response.end(content)
                        return
                    }
                })
            } else {
                response.writeHead(404, { 'Content-type': 'text/html' })
                response.end("Not Found")
            }

        } else if (request.method === 'POST') {
            if (pathname == '/file') {
                if (query.filename && query.content) {
                    if(!isCorrectFileExtension(response,query)) return
                    if (!fs.existsSync(logsFile)) {
                        createLogFile()
                    }
                    fs.writeFile(`./file/${query.filename}`, query.content, function (err, content) {
                        if (err) {
                            response.writeHead(404, { 'Content-type': 'text/html' })
                            response.end(`Something goes wrong`)
                        }
                        writeLogs(`New file with name ${query.filename} saved`)
                        response.writeHead(200, { 'Content-type': 'text/html' })
                        response.end()
                    });
                } else {
                    response.writeHead(400, { 'Content-type': 'text/html' })
                    response.end("Wrong input params")
                }
            }
            else {
                response.writeHead(404, { 'Content-type': 'text/html' })
                response.end("Not Found")
            }
        }
        else{
            response.writeHead(404, { 'Content-type': 'text/html' })
            response.end("Not Found")
        }

    }).listen(process.env.PORT || 8080)
}